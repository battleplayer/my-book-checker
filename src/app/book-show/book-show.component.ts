import {Component, Input} from '@angular/core';
import {BookModel} from '../../models/book.model';

@Component({
  selector: 'app-book-show',
  templateUrl: './book-show.component.html',
  styleUrls: ['./book-show.component.less']
})
export class BookShowComponent  {
  constructor() { }

  @Input() book: BookModel;

  backCorrectBase64(base64: string) {
    return `data:image/*;base64, ${base64}`;
  }

}
