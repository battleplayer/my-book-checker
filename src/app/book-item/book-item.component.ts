import {Component, EventEmitter, Input, Output} from '@angular/core';
import {BookModel} from '../../models/book.model';


@Component({
  selector: 'app-book-item',
  templateUrl: './book-item.component.html',
  styleUrls: ['./book-item.component.less']
})
export class BookItemComponent {
  @Input() book: BookModel;

  @Output() getId = new EventEmitter<number>();

  getBase64(base64: string): string {
    return `data:image/*; base64, ${base64}`;
  }
}
